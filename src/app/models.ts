
export class Choice {
  constructor(public value: string, public correct?: boolean){}
}

export class Question {
  constructor(public label:string, public choices: Choice[], public image?:string){}
}

export class Quiz {
  constructor(public label: string, public name: string, public description: string, public fileName: string) {}
}
