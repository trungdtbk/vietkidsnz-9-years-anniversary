import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Choice, Question } from '../models';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent {
  @Input() question: Question | undefined;
  @Output() choiceMade = new EventEmitter<string>();

  randomizedChocies: Choice[] = [];

  onChoiceMadeEvent(value: string) {
    this.choiceMade.emit(value)
  }
}
