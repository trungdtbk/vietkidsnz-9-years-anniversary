import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { QuestionService } from '../question.service';
import { Question } from '../models';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit, OnDestroy {

  levelNameEncryption : Map<string, string> = new Map([
    ["WIyTypfLboceyvb", "level1"],
    ["LBCjuOixNfCTwKS", "level2"],
    ["bHCsDsqCBNQNWCf", "level3"],
    ["pjwfmIOGLdevbAO", "level4"],
    ["yrxSPUPRRSrGnJk", "level5"],
    ["IgmxoHuqsxyuEBr", "level6"],
    ["qfSzgkTWrdhHJZL", "level7"],
    ["KsEOswhunROwmJU", "level8"],
    ["XBLSCdXNHYsygTJ", "level9"],
    ["XrmmtWxxvYNQLum", "level10"],
    ["adeZHfZLPIGvBUG", "test-level1"],
    ["sLSjqZvdHKJqtxW", "test-level2"]
  ]);

  categoryId : string|null = "";
  sub : any;

  selectedChoice = "";
  questions : Question[] = [];
  currentQuestionIndex  = 0;
  currentQuestion : Question |undefined;

  displayLevelCode  = false;
  postponeCheck = false;

  constructor(private route:ActivatedRoute, private _snackBar: MatSnackBar, private questionService: QuestionService) {}

  onQuestionAnswer(answer: string) {
    this.selectedChoice = answer
    if (!this.postponeCheck) {
      if (this.checkAnswer(this.selectedChoice)) {
        this._snackBar.open("CORRECT", "", {panelClass: ['custom-snackbar-correct']})
        this.currentQuestionIndex += 1
        if (this.currentQuestionIndex < this.questions.length) {
          setTimeout(() => {
            this._snackBar.dismiss();
            this.setCurrentQuestion()
          }, 500)
        }
      } else {
        this._snackBar.open("WRONG", "", {panelClass: ['custom-snackbar-wrong']})
        this.postponeCheck = true;
        setTimeout(() => {
          this._snackBar.dismiss();
          this.setCurrentQuestion();
          this.postponeCheck = false;
        }, 1500)
      }
    }
  }

  setCurrentQuestion() {
    const question = this.questions[this.currentQuestionIndex]
    this.randomizeChoices(question)
    this.currentQuestion = question
  }

  randomizeChoices(question: Question) {
    let randomIndex;
    let temporaryValue;
    const choices = question?.choices
    if(!Array.isArray(choices)) {
      return choices;
    }
    let currentIdx = choices.length
    while (currentIdx !== 0 && currentIdx != undefined ) {
      randomIndex = Math.floor(Math.random()*currentIdx);
      currentIdx -= 1;

      temporaryValue = choices[currentIdx];
      choices[currentIdx] = choices[randomIndex];
      choices[randomIndex] = temporaryValue
    }
    return choices
  }

  checkAnswer(answer: string): boolean {
    if (this.currentQuestion && Array.isArray(this.currentQuestion.choices)) {
      const choices = this.currentQuestion.choices
      for (const choice of choices) {
        if (choice.correct && choice.value == answer) {
          return true
        }
      }
    }
    return false
  }

  getEntries(): [string, string][] {
    return Array.from(this.levelNameEncryption.entries())
  }

  ngOnInit() {
    const routeParams = this.route.paramMap;
    this.sub = routeParams.subscribe(params => {
      this.categoryId = params.get('categoryId');
      let categoryId = this.categoryId
      if (categoryId == null) {
        categoryId = ""
      }

      console.log(categoryId)

      if (categoryId == "displayLevelCode$$$") {
        this.displayLevelCode = true;
      } else {
        const levelName = this.levelNameEncryption.get(categoryId)
        console.log(categoryId, levelName)
        if (levelName) {
          this.questionService.getQuestions(levelName).subscribe(
            items => {
              for(const element of items) {
                this.questions.push(new Question(element.label, element.choices, element.image))
              }
              if (this.questions.length > 0) {
                this.setCurrentQuestion()
              }
            })
          }
        }
      })
  } 

  ngOnDestroy() {
    this.sub.unsubcribe();
  }
}
